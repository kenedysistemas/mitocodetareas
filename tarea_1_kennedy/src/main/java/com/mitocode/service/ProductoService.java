package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Producto;

public interface ProductoService {

	public List<Producto> listar();
	public Producto listarPorId(Integer id);
	public Producto crear(Producto per);
	public Producto modificar(Producto per);
	public void eliminar(Integer id); 
	
}
