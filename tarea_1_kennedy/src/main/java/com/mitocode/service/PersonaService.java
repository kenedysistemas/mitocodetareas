package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Persona;

public interface PersonaService {

	public List<Persona> listar();
	public Persona listarPorId(Integer id);
	public Persona crear(Persona per);
	public Persona modificar(Persona per);
	public void eliminar(Integer id); 
	
}
