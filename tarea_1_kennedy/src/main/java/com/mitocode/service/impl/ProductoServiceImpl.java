package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ProductoDAO;
import com.mitocode.model.Producto;
import com.mitocode.service.ProductoService;

/*Importante agregar el service en la implementacion de lo contrario no loencunetra el controladorS */
@Service
public class ProductoServiceImpl implements ProductoService
{
	@Autowired
	private ProductoDAO dao;

	@Override
	public List<Producto> listar() {
		return dao.findAll();
	}

	@Override
	public Producto listarPorId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public Producto crear(Producto per) {
		return dao.save(per);
	}

	@Override
	public Producto modificar(Producto per) {
		return dao.save(per);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
		
	}

	

}
