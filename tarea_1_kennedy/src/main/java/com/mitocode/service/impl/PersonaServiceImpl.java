package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.PersonaDAO;
import com.mitocode.model.Persona;
import com.mitocode.service.PersonaService;

/*Importante agregar el service en la implementacion de lo contrario no loencunetra el controladorS */
@Service
public class PersonaServiceImpl implements PersonaService
{
	@Autowired
	private PersonaDAO dao;

	@Override
	public List<Persona> listar() {
		return dao.findAll();
	}

	@Override
	public Persona listarPorId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public Persona crear(Persona per) {
		return dao.save(per);
	}

	@Override
	public Persona modificar(Persona per) {
		return dao.save(per);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
		
	}

}
