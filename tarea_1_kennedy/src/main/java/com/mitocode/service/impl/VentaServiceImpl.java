package com.mitocode.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.DetalleVentaDAO;
import com.mitocode.dao.VentaDAO;
import com.mitocode.dto.DetalleVentaDTO;
import com.mitocode.dto.ProductoDTO;
import com.mitocode.model.Venta;
import com.mitocode.service.VentaService;

/*Importante agregar el service en la implementacion de lo contrario no loencunetra el controladorS */
@Service
public class VentaServiceImpl implements VentaService
{
	@Autowired
	private VentaDAO dao;
	
	@Autowired
	private DetalleVentaDAO dvdao;

	@Override
	public List<Venta> listar() {
		return dao.findAll();
	}

	@Override
	public Venta listarPorId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public Venta crear(Venta ven) {
		return dao.save(ven);
	}

	@Override
	public Venta modificar(Venta ven) {
		return dao.save(ven);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
		
	}

 
	@Transactional
	@Override
	public Venta registrarTransaccional(DetalleVentaDTO detalleVentaDTO) {

		dao.save(detalleVentaDTO.getVenta());
		System.out.println("Regsitrao la venta::"+detalleVentaDTO.getVenta().getIdVenta());
		
		//detalleVentaDTO.getListProducto().forEach(e -> dvdao.registrar(detalleVentaDTO.getVenta().getIdVenta(), e.getProducto().getIdProducto(),e.getCantidad()));
		for (ProductoDTO obj : detalleVentaDTO.getListProducto()) {
			dvdao.registrar(detalleVentaDTO.getVenta().getIdVenta(), obj.getIdProducto(), obj.getCantidad());
		}
		
		return detalleVentaDTO.getVenta();
	}

	

}
