package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.DetalleVentaDAO;
import com.mitocode.model.DetalleVenta;
import com.mitocode.service.DetalleVentaService;

/*Importante agregar el service en la implementacion de lo contrario no loencunetra el controladorS */
@Service
public class DetalleVentaServiceImpl implements DetalleVentaService
{
	@Autowired
	private DetalleVentaDAO dao;

	@Override
	public List<DetalleVenta> listar() {
		return dao.findAll();
	}

	@Override
	public DetalleVenta listarPorId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public DetalleVenta crear(DetalleVenta ven) {
		return dao.save(ven);
	}

	@Override
	public DetalleVenta modificar(DetalleVenta ven) {
		return dao.save(ven);
	}

	@Override
	public void eliminar(Integer id) {
		dao.delete(id);
		
	}

	

}
