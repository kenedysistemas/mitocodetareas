package com.mitocode.service;

import java.util.List;

import com.mitocode.model.DetalleVenta;

public interface DetalleVentaService {

	public List<DetalleVenta> listar();
	public DetalleVenta listarPorId(Integer id);
	public DetalleVenta crear(DetalleVenta per);
	public DetalleVenta modificar(DetalleVenta per);
	public void eliminar(Integer id); 
	
}
