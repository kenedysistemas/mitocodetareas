package com.mitocode.service;

import java.util.List;

import com.mitocode.dto.DetalleVentaDTO;
import com.mitocode.model.Venta;

public interface VentaService {

	public List<Venta> listar();
	public Venta listarPorId(Integer id);
	public Venta crear(Venta per);
	public Venta modificar(Venta per);
	public void eliminar(Integer id); 
	public Venta registrarTransaccional(DetalleVentaDTO ventaDTO);
}
