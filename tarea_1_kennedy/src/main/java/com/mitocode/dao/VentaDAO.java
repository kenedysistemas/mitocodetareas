package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.Venta;

public interface VentaDAO extends JpaRepository<Venta, Integer>{

}
