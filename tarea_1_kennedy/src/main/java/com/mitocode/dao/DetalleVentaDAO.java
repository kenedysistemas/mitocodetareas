package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.DetalleVenta;

public interface DetalleVentaDAO extends JpaRepository<DetalleVenta, Integer>{

	@Modifying
	@Query(value = "INSERT INTO detalle_venta(id_venta, id_producto, cantidad) VALUES (:idVenta, :idProducto, :cantidad)", nativeQuery = true)
	Integer registrar(@Param("idVenta") Integer idVenta, @Param("idProducto") Integer idProducto,@Param("cantidad") Integer cantidad);

}
