package com.mitocode.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class DetalleVentaPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
 
	@ManyToOne
	@JoinColumn(name="id_venta",nullable=false)
	private Venta venta;
	
	@ManyToOne
	@JoinColumn(name="id_producto",nullable=false)
	private Producto producto;

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((venta == null) ? 0 : venta.hashCode());
		result = prime * result + ((producto == null) ? 0 : producto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalleVentaPK other = (DetalleVentaPK) obj;
		if (venta == null) {
			if (other.venta != null)
				return false;
		} else if (!venta.equals(other.venta))
			return false;
		if (producto == null) {
			if (other.producto != null)
				return false;
		} else if (!producto.equals(producto))
			return false;
		return true;
	}
	
	
}
