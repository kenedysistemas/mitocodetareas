package com.mitocode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tarea1KennedyApplication {

	public static void main(String[] args) {

		SpringApplication.run(Tarea1KennedyApplication.class, args);
	}
}
