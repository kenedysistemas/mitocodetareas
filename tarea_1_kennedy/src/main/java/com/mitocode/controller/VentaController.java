package com.mitocode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.dto.DetalleVentaDTO;
import com.mitocode.model.DetalleVenta;
import com.mitocode.model.Venta;
import com.mitocode.service.DetalleVentaService;
import com.mitocode.service.VentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {

	@Autowired
	private VentaService service;
	
	@Autowired
	private DetalleVentaService dService;
	
	@GetMapping
	public List<Venta> listar() {
		return service.listar();
	}
	
	@GetMapping(value="/{id}")
	public Venta listaPorId(@PathVariable("id") Integer id) {
		return service.listarPorId(id);
	}
	
	@GetMapping(value="/detalle-ventas")
	public List<DetalleVenta> listaDetalle() {
		return dService.listar();
	}
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public Venta registrar(@RequestBody DetalleVentaDTO detalleventaDTO) {
		return service.registrarTransaccional(detalleventaDTO);
	}
	
	@PutMapping(consumes = "application/json", produces = "application/json")
	public Venta actualizar(@RequestBody Venta ven) {
		return service.modificar(ven);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id")Integer id) {
		service.eliminar(id);
	}
}
