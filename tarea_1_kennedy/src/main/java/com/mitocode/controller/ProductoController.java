package com.mitocode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Producto;
import com.mitocode.service.ProductoService;

@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private ProductoService service;
	
	@GetMapping
	public List<Producto> listar() {
		return service.listar();
	}
	
	@GetMapping(value="/{id}")
	public Producto listaPorId(@PathVariable("id") Integer id) {
		return service.listarPorId(id);
	}
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public Producto registrar(@RequestBody Producto pro) {
		return service.crear(pro);
	}
	
	@PutMapping(consumes = "application/json", produces = "application/json")
	public Producto actualizar(@RequestBody Producto pro) {
		return service.modificar(pro);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id")Integer id) {
		service.eliminar(id);
	}
}
