package com.mitocode.test;

import java.time.LocalDateTime;

import com.google.gson.Gson;
import com.mitocode.model.DetalleVenta;
import com.mitocode.model.Persona;
import com.mitocode.model.Producto;
import com.mitocode.model.Venta;

public class test1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Creando Json Persona");
		Persona persona = new Persona(); 
		persona.setApellidos("palomino");
		persona.setNombres("nombres");
		persona.setIdPersona(1);
		
		
		Producto pro = new Producto();
		pro.setIdProducto(1);
		pro.setMarca("marca");
		pro.setNombre("nombre");
		
		LocalDateTime dateTime = LocalDateTime.now();
		Venta v = new Venta();
		
		v.setFecha(dateTime);
		v.setIdVenta(1);
		v.setImporte(200.00);
		v.setPersona(persona);
		
		DetalleVenta dv = new DetalleVenta();
		
//		s
		
		Gson gson = new Gson();
		
		System.out.println(gson.toJson(dv));
	}

}
