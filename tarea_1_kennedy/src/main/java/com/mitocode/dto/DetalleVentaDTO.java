package com.mitocode.dto;

import java.util.List;

import com.mitocode.model.Venta;

public class DetalleVentaDTO {

	private Venta venta;
	private List<ProductoDTO> listProducto;
	
	public Venta getVenta() {
		return venta;
	}
	public void setVenta(Venta venta) {
		this.venta = venta;
	}
	public List<ProductoDTO> getListProducto() {
		return listProducto;
	}
	public void setListProducto(List<ProductoDTO> listProducto) {
		this.listProducto = listProducto;
	}

}
